import 'package:flutter_test/flutter_test.dart';

import 'package:floating_bottom_nav_bar/floating_bottom_nav_bar.dart';

void main() {
  
  group("controller", () {
    
    final FloatingBottomNavBarController controller = FloatingBottomNavBarController();
    
    test("add listener", () {
      void listener(int index) => print('tapez $index');
      controller.listen(listener);
      expect(controller.listeners.contains(listener), true);
    });

    test("remove listener", () {
      void listener(int index) => print('tapez $index');
      controller.removeListener(listener);
      expect(controller.listeners.contains(listener), false);
    });
    
    
  });
  
  
}
