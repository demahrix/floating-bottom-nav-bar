
typedef _Listener = void Function(int);

class FloatingBottomNavBarController {

  int _currentIndex;
  Set<_Listener> _listeners = Set();

  int get index => _currentIndex;
  List<_Listener> get listeners => _listeners.toList(growable: false);

  void Function(int) change;
  void Function(int index,bool status) setNotifyState;

  void listen(_Listener listener) {
    _listeners.add(listener);
  }

  void dispatch(int index) {
    _currentIndex = index;
    for (final listener in _listeners)
      listener(index);
  }

  bool removeListener(_Listener _listener) {
    return _listeners.remove(_listener);
  }

  void dispose() {
    _listeners.clear();
    _listeners = null;
    _currentIndex = null;
  }

}

