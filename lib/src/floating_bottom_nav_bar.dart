import 'package:flutter/material.dart';
import 'package:floating_bottom_nav_bar/floating_bottom_nav_bar.dart';

import 'floating_bottom_nav_bar_item.dart';

// FIXME ajouter la possibilité de modifier  l'ombres
class FloatingBottomNavbar extends StatefulWidget {

  final FloatingBottomNavBarController controller;
  final List<IconData> items;
  final double height;
  final EdgeInsets margin;
  final Color backgroundColor;
  final Color selectedColor;
  final Color unselectedColor;
  final int initialIndex;
  final ValueChanged<int> onChanged;
  final List<bool> notifyState;

  FloatingBottomNavbar({
    this.controller,
    this.items,
    this.height = 56.0,
    this.margin = const EdgeInsets.symmetric(horizontal: 15.0),
    this.backgroundColor,
    this.selectedColor,
    this.unselectedColor,
    this.initialIndex = 0,
    this.onChanged,
    this.notifyState
  }): assert(height != null),
      assert(items != null),
      assert(backgroundColor != null),
      assert(selectedColor != null),
      assert(unselectedColor != null),
      assert(initialIndex != null),
      assert(notifyState == null || items.length == notifyState.length);

  @override
  _FloatingBottomNavbarState createState() => _FloatingBottomNavbarState();
}

// FIXME: add shaddow
class _FloatingBottomNavbarState extends State<FloatingBottomNavbar> {

  int _selectedIndex = 0;
  List<bool> _notifyState;

  @override
  void initState() {
    super.initState();

    final controller = widget.controller;
    if (controller != null) {
      controller.change = _change;
      controller.setNotifyState = _changeNotifyState;
    }

    _notifyState = widget.notifyState?.toList(growable: false) ?? List.filled(widget.items.length, false);
  }

  void _change(int index) {
    if (index != _selectedIndex) {
      setState(() {
        _selectedIndex = index;
        bool state = _notifyState[index];
        if (state)
          _notifyState[index] = false;
      });
      widget?.onChanged(index);
      widget?.controller?.dispatch(index);
    }
  }

  void _changeNotifyState(int index, bool state) {
    bool actualState = _notifyState[index];
    if (actualState != state) {
      setState(() { _notifyState[index] = state; });
    }
  }


  @override
  Widget build(BuildContext context) {

    final double radius = widget.height / 2;

    return Container(
      height: widget.height,
      margin: widget.margin,
      padding: EdgeInsets.symmetric(horizontal: radius),
      decoration: BoxDecoration(
        color: widget.backgroundColor,
        boxShadow: kElevationToShadow[4],
        borderRadius: BorderRadius.circular(radius)
      ),
      child: Row(
        children: _itemWidgets,
      ),
    );
  }

  List<FloatingBottomNavBarItem> get _itemWidgets {
    return List.generate(widget.items.length, (index) {

      final item = widget.items[index];
      final bool selected = index == _selectedIndex;

      return FloatingBottomNavBarItem(
        onTap: () => _change(index),
        icon: item,
        color: selected ? widget.selectedColor : widget.unselectedColor,
        hasIndicator: selected,
        notify: _notifyState[index],
      );
    });
  }

}