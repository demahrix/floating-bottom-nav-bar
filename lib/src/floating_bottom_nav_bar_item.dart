import 'package:flutter/material.dart';

class FloatingBottomNavBarItem extends StatelessWidget {

  final IconData icon;
  final Color color;
  final bool hasIndicator;
  final bool notify;
  final VoidCallback onTap;

  FloatingBottomNavBarItem({
    this.icon,
    this.color,
    this.hasIndicator,
    this.notify,
    this.onTap
  }): assert(icon != null),
      assert(color != null),
      assert(hasIndicator != null),
      assert(notify != null),
      assert(onTap != null);

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: GestureDetector(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            // color: Colors.red
          ),
          child: Stack(
            children: [

              Center(
                child: Icon(
                  icon,
                  color: color,
                ),
              ),

              if (notify)
                Align( // PART: notify
                  alignment: Alignment(0.35, -0.35),
                  child: Container(
                    width: 10.0,
                    height: 10.0,
                    decoration: BoxDecoration(
                      color: Colors.red,
                      shape: BoxShape.circle
                    ),
                  ),
                ),

              if (hasIndicator)
                Align( // PART: inidicator
                  alignment: Alignment(0, 1),
                  child: Container(
                    height: 3.0,
                    decoration: BoxDecoration(
                      color: color,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(3.0),
                        topRight: Radius.circular(3.0)
                      )
                    ),
                  ),
                )

            ],
          ),
        ),
      ),
    );
  }
}